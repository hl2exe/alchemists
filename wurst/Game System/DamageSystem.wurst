package DamageSystem

import IndexingSystem
import DamageConstants
import TextSystem

@configurable let ARMOR_DAMAGE_REDUCTION_MULTIPLIER = 0.06

// TODO: add listener for damage callback (during damage computation): before computation, after computation, after damage
public class DamageSystem

    private static function damageToString(real damage, DAMAGE_TYPE_ENUM damageType) returns string
        let colour = DAMAGE_TYPE_COLOURS.get(damageType)
        let typeName = DAMAGE_TYPE_NAMES.get(damageType).toUpperCase()
        let damageStr = colour + typeName + ": " + R2SW(damage, 5, 2) + "|r"
        return damageStr

    static function reductionFormula(real armorValue) returns real
        return ( (ARMOR_DAMAGE_REDUCTION_MULTIPLIER * armorValue) / ( 1 + ARMOR_DAMAGE_REDUCTION_MULTIPLIER * armorValue))

    static function applyCombatDamage(RegisteredUnit hit, real damage, DAMAGE_TYPE_ENUM damageType)
        let hitArmor = hit.getArmorData()
        let hitHandle = hit.getUnitHandle()
        let hitLife = hitHandle.getState(UNIT_STATE_LIFE)
        let hitMaxLife = hitHandle.getState(UNIT_STATE_MAX_LIFE)

        // print("[DamageSystem] Damage to apply: {0}".format(damage.toString()))

        // Armor base reduction
        let armorReduction =  DamageSystem.reductionFormula(hitArmor.getValue())
        real damageToApply = damage - armorReduction

        // print("[DamageSystem] Armor reduced damage ({1}): {0}".format(damageToApply.toString(), armorReduction.toString()))

        // Damage type reduction
        let attack_armor_key = "{0}->{1}".format(DAMAGE_TYPE_NAMES.get(damageType), DAMAGE_TYPE_NAMES.get(hitArmor.getArmorType()))
        let type_reduction = REDUCTION_TABLE.get(attack_armor_key)

        damageToApply = type_reduction * damageToApply

        // print("[DamageSystem] Element modified damage ({1}): {0}".format(damageToApply.toString(), type_reduction.toString()))

        applyText(hitHandle, DamageSystem.damageToString(damageToApply, damageType), 0., 1.5)

        if hitMaxLife <= hitLife - damageToApply
            SetUnitState(hitHandle, UNIT_STATE_LIFE, hitMaxLife)
        else
            SetUnitState(hitHandle, UNIT_STATE_LIFE, hitLife - damageToApply)

        // print("[DamageSystem] Hit state life: {0}".format(hitHandle.getState(UNIT_STATE_LIFE).toString()))

    static function calculateCombatDamage(RegisteredUnit dealer, RegisteredUnit target)
        let attackData = dealer.getAttackData()
        let rngDamage = GetRandomReal(attackData.getMinDamage(), attackData.getMaxDamage())
        let hitLife = target.getUnitHandle().getState(UNIT_STATE_LIFE)
        let hitMaxLife = target.getUnitHandle().getState(UNIT_STATE_MAX_LIFE)

        if hitLife >= hitMaxLife - 1
            // print("[DamageSystem] Resetting to max life!")
            SetUnitState(target.getUnitHandle(), UNIT_STATE_LIFE, hitMaxLife)

        attackData.updateDamage(rngDamage)

        // This must be done since in Wc3 the minimum attack/armor damage is 1!
        SetUnitState(target.getUnitHandle(), UNIT_STATE_LIFE, hitLife + 1)
        
        applyCombatDamage(target, rngDamage, attackData.getAttackType())


