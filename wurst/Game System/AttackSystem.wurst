package AttackSystem

import DamageConstants
import ClosureEvents
import ClosureTimers
import ClosureForGroups

import ExampleMissileObjects


public class Attack

    private static constant int ORDER_ATTACK = 851983
    
    private static constant int ORDER_AUTO_ACQUIRED = 0

    private real minDamage
    
    private real maxDamage

    private DAMAGE_TYPE_ENUM attackType

    private unit unitHandle

    construct(real minDamage, real maxDamage, DAMAGE_TYPE_ENUM attackType)
        this.minDamage = minDamage
        this.maxDamage = maxDamage
        this.attackType = attackType

    function attachUnit(unit u)
        this.unitHandle = u
        this.activateOrderReplace()

        let damage = GetRandomReal(minDamage, maxDamage)
        SetHeroStr(u, damage.toInt(), true)

    static function orderReplaceAction()
        let unitHandle = GetTriggerUnit()
        let order = GetUnitCurrentOrder(unitHandle)
        if unitHandle.isAlive()
            print("[Unit {0}] Current order: {1}".format(unitHandle.getName(), OrderId2String(order)))
            forUnitsInRange(unitHandle.getPos(), 500.0) (unit targetHandle) ->
                if targetHandle.isAlive()
                    print("[Unit {0}] Attacking unit: {1}".format(unitHandle.getName(), targetHandle.getName()))
                    IssuePointOrder(unitHandle, "breathoffire", targetHandle.getX(), targetHandle.getY())

    // TODO: register trigger at instance level and dispatch it when unit dies
    private function activateOrderReplace()
        let myTrig = CreateTrigger()
        TriggerRegisterUnitEvent(myTrig, this.unitHandle, EVENT_UNIT_ACQUIRED_TARGET)
        TriggerRegisterUnitEvent(myTrig, this.unitHandle, EVENT_UNIT_TARGET_IN_RANGE)
        TriggerAddAction(myTrig, function orderReplaceAction)

    function getMinDamage() returns real
        return this.minDamage

    function setMinDamage(real minDamage)
        this.minDamage = minDamage

    function getMaxDamage() returns real
        return this.maxDamage

    function setMaxDamage(real maxDamage)
        this.maxDamage = maxDamage

    function getAttackType() returns DAMAGE_TYPE_ENUM
        return this.attackType

    function updateDamage(real damage)
        SetHeroStr(this.unitHandle, damage.toInt(), true)

    static function getRandomAttack(DAMAGE_TYPE_ENUM armorType) returns Attack
        let rngMinValue = GetRandomReal(1, 5.)
        let rngMaxValue = GetRandomReal(5., 10.)
        let rngAttack = new Attack(rngMinValue, rngMaxValue, armorType)

        return rngAttack

    function getAttackTypeName() returns string
        return DAMAGE_TYPE_NAMES.get(this.attackType)
