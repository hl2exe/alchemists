package HeroAttack

import ChannelAbilityPreset
import public Assets
import MissileSystem
import ClosureEvents
import ClosureForGroups
import DamageSystem
import DamageConstants
import IndexingSystem

import MovementLibrary

// Constants

@configurable public let BASE_DAMAGE = 100.
@configurable public let SPEED = 12.
@configurable public let HEIGHT = 150.
@configurable public let RANGE = 1000.
@configurable public let COLLISION_RANGE = 50.

@configurable public let SPELL_EFFECT = Abilities.dispelMagicTarget

@configurable public let SPELL_ICON = Icons.bTNAbsorbMagic
@configurable public let SPELL_NAME = "Magic attack"
@configurable public let SPELL_TT_NORMAL = "Magic attack"
@configurable public let SPELL_TT_EXTENDED = "Fires a linear missile in a direction"
@configurable public let SPELL_LEARN = ""

// Compile Time

public let HERO_ATTACK_ID = compiletime(ABIL_ID_GEN.next())
public let HERO_ATTACK_DUMMY_ID = compiletime(UNIT_ID_GEN.next())

@compiletime function genObj()
    new AbilityDefinitionBrewmasterBreathofFire(HERO_ATTACK_ID)
    ..setDummyAbility()
    ..setName(SPELL_NAME)
    ..presetTooltipNormal(lvl -> SPELL_TT_NORMAL)
    ..presetTooltipNormalExtended(lvl -> SPELL_TT_EXTENDED)
    ..presetIcon(SPELL_ICON)
    ..presetButtonPosNormal(0, 0)
    ..presetHotkey("A")
    ..setButtonPositionNormalX(3)
    ..setButtonPositionNormalY(0)
    ..setEffectSound("")
    ..setHeroAbility(true)
    ..presetTooltipLearn(lvl -> SPELL_LEARN)
    ..setArtCaster("")
    ..setArtTarget("")
    ..setMissileArt("")
    ..presetDamage(lvl -> 0)
    ..setCooldown(0, 0.)
    ..setCooldown(1, 0.)

@compiletime function genDummy()
    new UnitDefinition(HERO_ATTACK_DUMMY_ID, UnitIds.footman)
    ..setName(SPELL_NAME)
    ..setUpgradesUsed("")
    ..setAttacksEnabled(0)
    ..setCollisionSize(5.0)
    ..setFoodCost(0)
    ..setModelFile(Abilities.dragonHawkMissile)
    ..setIconGameInterface(SPELL_ICON)
    ..setAnimationBlendTimeseconds(0.0)
    ..setAnimationCastBackswing(0.0)
    ..setMaximumPitchAngledegrees(0.0)
    ..setMaximumRollAngledegrees(0.0)
    ..setArtSpecial("")
    ..setScalingValue(0.9)
    ..setHideMinimapDisplay(true)
    ..setNameEditorSuffix("[HeroAttack]")
    ..setMovementHeight(60.)
    ..setNormalAbilities(commaList(AbilityIds.invulnerable, AbilityIds.locust))
    ..setAttack1ShowUI(false)
    ..setAttack2ShowUI(false)
    ..setAcquisitionRange(0.)
    ..setAttacksEnabled(0)


// Spell

function nearbyFilter() returns boolean
    let indexer = Indexer.getInstance()
    let u = GetFilterUnit()

    return u.isAlive() and indexer.isRegistered(u)


init
    EventListener.onPointCast(HERO_ATTACK_ID) (unit caster, vec2 target) ->
        let casterPos = caster.getPos()
        let castingAngle = casterPos.angleTo(target)
        let dummyPos = casterPos.polarOffset(castingAngle, 20).toVec3()
        let dummy = createUnit(caster.getOwner(), HERO_ATTACK_DUMMY_ID, dummyPos, castingAngle)
        let movementType = new LinearMovement(SPEED)
        let missile = new Missile(caster, null, dummy, movementType)
        let indexer = Indexer.getInstance()

        // Add on loop actions

        // Add on update actions
        missile.addOnUpdate() (real increment) ->
            movementType.update(missile.getMissileHandler(), target.toVec3(), false, casterPos.toVec3())

        // Add exit conditions
        missile.addOnCheck() () ->
            let missilePos = missile.getMissileHandler().getPos()

            // Check max range 
            let maxRangeAchieved = missilePos.distanceTo(casterPos) >= RANGE
        
            // Check destroyed
            let gotDestroyed = missile.getMissileHandler() == null

            // Check nearby enemy units
            let g = CreateGroup()
            GroupEnumUnitsInRange(g, missilePos.x, missilePos.y, COLLISION_RANGE, Filter(function nearbyFilter))
            unit nearest = null
            var bestDist = REAL_MAX
            for u from g
                let distSq = missilePos.distanceToSq(u.getPos())
                if distSq < bestDist
                    nearest = u
                    bestDist = distSq
            g.destr()
            
            let hasHit = nearest != null and GetOwningPlayer(nearest).isEnemyOf(caster.getOwner())

            return maxRangeAchieved or gotDestroyed or hasHit
                

        // Add on collision actions before destruction
        missile.addOnCollision() -> 
            let missilePos = missile.getMissileHandler().getPos()

            forUnitsInRange(missilePos, COLLISION_RANGE) (unit u) ->
                if indexer.isRegistered(u) and u.isAlive()
                    let uReg = indexer.getUnitInfo(u)
                    DamageSystem.applyCombatDamage(uReg, BASE_DAMAGE, DAMAGE_TYPE_ENUM.PHYSICAL)

            flashEffect(SPELL_EFFECT, missilePos)
                

        missile.apply()